from setuptools import setup, find_packages

setup(
    name='InstanceStateManagement',
    version='0.1.0.0',
    readme='readme.md',
    description='Manage EC2 Instances with Lambda',
    long_description='readme',
    author='Joshua Bister',
    author_email='jbister@eastridge.com',
    packages=find_packages(exclude=('tests', 'docs')),
    entry_points = {
        'console_scripts': [
            'instancestatemanagement=src.cron_management.instance_scheduler:main'
        ]
    }
)
