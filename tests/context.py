# -*- coding: utf-8 -*-

import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import src.connections.AWSConnections as ec2
import src.cron_management.instance_scheduler as inst