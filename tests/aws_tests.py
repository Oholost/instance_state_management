import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import src.connections.AWSConnections as ec2
import src.cron_management.instance_scheduler as inst

import unittest
#from crontab import CronTab
from csv import DictWriter

from botocore.exceptions import ClientError
"""

class BasicTestSuite(unittest.TestCase):
    "Basic test cases."

    def test_absolute_truth_and_meaning(self):
        assert True
    
    def test_get_ids_from_tag(self):
        
        conn = ec2.EC2Management(region='us-west-2')
        ids = conn.get_ids_from_tag('Name',['Gitlab'])  
        assert ids == ['i-0fd048591dfacd4a7']
"""

class CLITests(unittest.TestCase):
    def setup(self):
        inst.set_environment_job('Test_Run', instances=['1111'], startup_shutdown='startup', Test=True)
        
    @unittest.skip
    def test_cli(self):
        valid_args = [
                '-r','-n', '1','-m','-s','-i', '-I','-R', '-T', '-e test', 
            ]
        args = inst.get_cli_args(valid_args)
        
        self.assertListEqual(args, valid_args)


    # Test handle failure to include all args with -r
    # def test_not_enough_args(self):

    def test_run_from_cli(self):
        args = '-r -e Test -T'
        sys.argv[1:] = args.split()
        actual = inst.main()
        expected = 0
        
        self.assertEqual(actual,expected)
        
        
    def test_handle_state_dry_run_invalid_instance_id(self):
        args = { 
            'set_state':'startup',
            'instances':['1111'],
            'env_name':'Test',
            'region':'us-west-2',
            'Test':True
        }

        expected = 'InvalidInstanceID.Malformed'
        actual = inst.handle_state(**args).response['Error']['Code']
        
        self.assertEqual(expected,actual)
        
    def test_handle_state_dry_run_valid_instance_id(self):
        valid_instance_id = 'i-0cf34b4e6f02ca6cf'
        
        args = { 
            'set_state':'startup',
            'instances':[valid_instance_id],
            'env_name':'Test',
            'region':'us-west-2',
            'Test':True
        }        

        expected = 0
        actual = inst.handle_state(**args)
        
        self.assertEqual(expected,actual)
        
        
    def test_handle_server_state_vs_test_instance(self):
        # Doesn't currently handle server state, only that the request got a response from aws
        valid_instance_id = 'i-0cf34b4e6f02ca6cf'
        
        args = { 
            'set_state':'startup',
            'instances':[valid_instance_id],
            'env_name':'Test',
            'region':'us-west-2',
        }
        ret = inst.handle_state(**args)
        self.assertIsNotNone(ret)
        
        
    def tearDown(self):
        inst.remove_environment_jobs('Test_Run')
        
        valid_instance_id = 'i-0cf34b4e6f02ca6cf'
        
        args = { 
            'set_state':'shutdown',
            'env_name':'Test',
            'region':'us-west-2',
        }        
        inst.handle_state(**args)
        

# TODO test new functions
class CronManagementTests(unittest.TestCase):
    
    def setUp(self):
        # Test 1
        testno = 'Test1'
        inst.set_environment_job(testno, instances= ('1111','2222','3333'), startup_shutdown='startup', Test=True)
        
        # Test 2
        testno = 'Test2'
        with open('test2.csv', 'w') as testcsv:
            d = DictWriter(testcsv, fieldnames=["instance_ids"])
            ids = [{'instance_ids': '1111'},
             {'instance_ids': '2222'},
             {'instance_ids': '3333'}]
            d.writeheader()
            d.writerows(ids)
            
        inst.set_environment_job(testno, instances_file='test2.csv', startup_shutdown='startup', Test=True)
        
        # Test 3
        testno = 'Test3'
        
        # Test 4 -- Test Job Is Invalid Assertion
        testno = 'Test4'
        
        #Test 5 -- Test that job is modified
        testno = 'Test5'
        
        # Test 6 -- Test running job
        testno = 'Test6'
        inst.set_environment_job(testno, instances= ('1111','2222','3333'), startup_shutdown='startup', Test=True)
        
        
    # Test 1    
    def test_set_environment_job_instances_param(self):
        testno = 'Test1'
        actual = len(list(inst.ret_env_job(testno)))
        expected = 1
        self.assertEqual(actual, expected)
    
    # Test 2
    # TODO: FIX HANDLE OF RET_ENV_JOB RETURN VALUE
    def test_set_environment_job_file_param(self):
        testno = 'Test2'
        actual = len([job for job in inst.ret_env_job(testno)])
        expected = 1
        self.assertEqual(actual, expected)
    
    # Outdated test
    # Test 3
    @unittest.expectedFailure
    def test_set_environment_job_exception(self):
        testno = 'Test3'
        self.assertRaises('No instances provided', inst.set_environment_job(testno, startup_shutdown='shutdown', Test=True))
    
    
    # Test 5
    def test_modify_existing_environment_job_time(self):
        testno = 'Test5'
        job_initial = list(inst.ret_env_job('Test1'))
        
        inst.modify_existing_environment_job_time('Test1', 0)
        
        job_changed = list(inst.ret_env_job('Test1'))
        print(job_initial)
        print(job_changed)
        
        self.assertNotEqual(job_initial, job_changed)
    
    
    # Test 6
    def test_run_job_expect_shutdown_dry_run(self):
        testno = 'Test6'
        inst.run_job_now(testno)
        
    
    def tearDown(self):
        inst.remove_environment_jobs('Test1')
        os.remove("test2.csv")
        inst.remove_environment_jobs('Test2')
        
if __name__ == "__main__":
    
    
    unittest.main()